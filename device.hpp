/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_DEVICE_HPP
#define CRUFT_VK_DEVICE_HPP

#include "./object.hpp"

#include "./fwd.hpp"

namespace cruft::vk {
    struct device : public descendant<device> {
        device (const physical_device&,
                const VkDeviceCreateInfo&);

        struct queue queue (uint32_t id);

        void flush (const VkMappedMemoryRange *first,
                    const VkMappedMemoryRange *last);

        void invalidate (const VkMappedMemoryRange *first,
                         const VkMappedMemoryRange *last);
    };
}

#endif
