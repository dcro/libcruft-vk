/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_SHADER_MODULE_HPP
#define CRUFT_VK_SHADER_MODULE_HPP

#include "./object.hpp"
#include "./fwd.hpp"

#include <filesystem>


namespace cruft::vk {
    struct shader_module : public owned<shader_module,device> {
        using create_t = create_info_t<native_type>;

        shader_module (device&, const std::filesystem::path &src);

    private:
        // used as a convenience to allow taking address of a temporary
        // create_info_t in the filesystem::path constructor.
        struct cookie;

        shader_module (device&, const cookie&);
    };
}

#endif
