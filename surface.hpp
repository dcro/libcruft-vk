/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_SURFACE_HPP
#define CRUFT_VK_SURFACE_HPP

#include "./vk.hpp"
#include "./instance.hpp"
#include "./object.hpp"


namespace cruft::vk {
    ///////////////////////////////////////////////////////////////////////////
    struct surface : public owned<surface,instance> {
        using owned::owned;
    };


    ///////////////////////////////////////////////////////////////////////////
    WRAP_FUNCTION (capabilities,  vkGetPhysicalDeviceSurfaceCapabilitiesKHR);
    WRAP_FUNCTION (formats,       vkGetPhysicalDeviceSurfaceFormatsKHR);
    WRAP_FUNCTION (present_modes, vkGetPhysicalDeviceSurfacePresentModesKHR);
};

#endif
