/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_DEVICE_MEMORY_HPP
#define CRUFT_VK_DEVICE_MEMORY_HPP

#include "./object.hpp"
#include "./fwd.hpp"

#include <cruft/util/view.hpp>

#include <cstddef>


namespace cruft::vk {
    ///////////////////////////////////////////////////////////////////////////
    struct map_t : public cruft::view<std::byte*> {
    public:
        using view::view;

        map_t (const map_t&) = delete;
        ~map_t ();

        map_t& operator= (const map_t&) = delete;
        map_t& operator= (std::nullptr_t);

        void destroy (device&, device_memory&);
    };


    //-------------------------------------------------------------------------
    map_t map (device&, device_memory&, int len);


    //////////////////////////////////////////////////////////////////////////
    struct owned_map_t : public map_t {
        owned_map_t (owned_ptr<device_memory>&, std::byte *first, std::byte *last);
        ~owned_map_t ();

        void destroy (void);

    private:
        owned_ptr<device_memory> &m_memory;
    };


    owned_map_t map (owned_ptr<device_memory>&, int len);


    ///////////////////////////////////////////////////////////////////////////
    struct device_memory : public owned<device_memory,device> {
        using owned::owned;

        void unmap (const device&);

        VkDeviceSize commitment (const device&) const;
    };
}

#endif
