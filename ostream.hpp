/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_OSTREAM_HPP
#define CRUFT_VK_OSTREAM_HPP

#include "./fwd.hpp"
#include "./vk.hpp"

#include <cruft/util/preprocessor.hpp>

#include <iosfwd>


///////////////////////////////////////////////////////////////////////////////
//std::ostream& operator<< (std::ostream&, VkQueueFlags);
std::ostream& operator<< (std::ostream&, VkExtent2D);
std::ostream& operator<< (std::ostream&, VkExtent3D);
std::ostream& operator<< (std::ostream&, VkPhysicalDeviceType);


///////////////////////////////////////////////////////////////////////////////
std::ostream& operator<< (std::ostream&, const VkLayerProperties&);

std::ostream& operator<< (std::ostream&, const VkPhysicalDeviceLimits&);
std::ostream& operator<< (std::ostream&, const VkPhysicalDeviceProperties&);

std::ostream& operator<< (std::ostream&, const VkQueueFamilyProperties&);


///////////////////////////////////////////////////////////////////////////////
namespace cruft::vk {
    #define CRUFT_VK_OSTREAM(T) \
     std::ostream&              \
    operator<< (std::ostream&, const cruft::vk::T&);

    VK_TYPE_MAP (CRUFT_VK_OSTREAM)

    #undef CRUFT_VK_OSTREAM
}

#endif
