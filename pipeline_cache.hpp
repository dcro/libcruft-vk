/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef __VK_PIPELINE_CACHE_HPP
#define __VK_PIPELINE_CACHE_HPP

#include "./object.hpp"
#include "./fwd.hpp"

namespace cruft::vk {
    struct pipeline_cache : public owned<pipeline_cache,device> {
        void merge (const device&,
                    const pipeline_cache *first,
                    const pipeline_cache *last);

        size_t get (const device&, void *dst, size_t len) const;
    };
}

#endif
