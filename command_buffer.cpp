/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017 Danny Robson <danny@nerdcruft.net>
 */

#include "./command_buffer.hpp"

#include "./event.hpp"
#include "./pipeline.hpp"

#include <cruft/util/cast.hpp>

using cruft::vk::command_buffer;


///////////////////////////////////////////////////////////////////////////////
void
command_buffer::reset (VkCommandBufferResetFlags flags)
{
    auto err = vkResetCommandBuffer (native (), flags);
    error::try_code (err);
}


///////////////////////////////////////////////////////////////////////////////
void
command_buffer::begin (const VkCommandBufferBeginInfo &info)
{
    auto err = vkBeginCommandBuffer (native (), &info);
    error::try_code (err);
}


//-----------------------------------------------------------------------------
void
command_buffer::begin (const VkRenderPassBeginInfo &info, VkSubpassContents contents)
{
    vkCmdBeginRenderPass (native (), &info, contents);
}


//-----------------------------------------------------------------------------
void
command_buffer::end (void)
{
    auto err = vkEndCommandBuffer (native ());
    error::try_code (err);
}


//-----------------------------------------------------------------------------
void
command_buffer::end_pass (void)
{
    vkCmdEndRenderPass (native ());
}


///////////////////////////////////////////////////////////////////////////////
template <size_t N>
void
command_buffer::execute (const command_buffer (&buffers)[N])
{
    vkCmdExecuteCOmmands (native (), N, buffers);
}


//-----------------------------------------------------------------------------
void
command_buffer::next_subpass (const VkSubpassContents contents)
{
    vkCmdNextSubpass (native (), contents);
}


///////////////////////////////////////////////////////////////////////////////
template <cruft::vk::bindpoint BindpointV>
void
command_buffer::bind (const pipeline<BindpointV> &p)
{
    vkCmdBindPipeline (native (), static_cast<VkPipelineBindPoint> (BindpointV), p.native ());
}


template void command_buffer::bind(const pipeline<bindpoint::GRAPHICS>&);
template void command_buffer::bind(const pipeline<bindpoint::COMPUTE>&);


///////////////////////////////////////////////////////////////////////////////
void
command_buffer::set (const event &ev, VkPipelineStageFlags flags)
{
    vkCmdSetEvent (native (), ev.native (), flags);
}


//-----------------------------------------------------------------------------
void
command_buffer::reset (const event &ev, VkPipelineStageFlags flags)
{
    vkCmdResetEvent (native (), ev.native (), flags);
}


//-----------------------------------------------------------------------------
void
command_buffer::wait (cruft::view<const event*> events,
                      VkPipelineStageFlags src_mask, 
                      VkPipelineStageFlags dst_mask, 
                      cruft::view<const VkMemoryBarrier*> VkMemoryBarriers,
                      cruft::view<const VkBufferMemoryBarrier*> VkBufferMemoryBarriers,
                      cruft::view<const VkImageMemoryBarrier*> VkImageMemoryBarriers)
{
    vkCmdWaitEvents (
        native (),
        cruft::cast::lossless<uint32_t> (events.size ()), &events.cbegin ()->native (),
        src_mask, dst_mask,
        cruft::cast::lossless<uint32_t> (VkMemoryBarriers.size ()), VkMemoryBarriers.cbegin (),
        cruft::cast::lossless<uint32_t> (VkBufferMemoryBarriers.size ()), VkBufferMemoryBarriers.cbegin (),
        cruft::cast::lossless<uint32_t> (VkImageMemoryBarriers.size ()), VkImageMemoryBarriers.cbegin ());
}
