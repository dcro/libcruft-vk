/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_BUFFER_HPP
#define CRUFT_VK_BUFFER_HPP

#include "./object.hpp"
#include "./fwd.hpp"

namespace cruft::vk {
    struct buffer : public owned<buffer,device> {
        using owned::owned;
    };
}

#endif
