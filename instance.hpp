/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "./object.hpp"

#include "./vk.hpp"
#include "./traits.hpp"
#include "./except.hpp"

#include <cruft/util/view.hpp>

#include <set>
#include <string>

namespace cruft::vk {
    struct instance : public root<instance> {
        struct create_info_t : public VkInstanceCreateInfo {
            create_info_t ();
            create_info_t (const VkInstanceCreateInfo&);
        };

        struct application_info_t : public VkApplicationInfo {
            application_info_t ();
            application_info_t (const VkApplicationInfo&);
        };

        instance ();
        instance (cruft::view<const char**> layers,
                  cruft::view<const char**> extensions);
        instance (const create_info_t &info);

        template <typename FunctionT>
        FunctionT
        proc (const char *name)
        {
            auto ret = vkGetInstanceProcAddr (native (), name);
            if (!ret)
                throw vk::invalid_argument ("invalid procedure name");

            return reinterpret_cast<FunctionT> (ret);
        }

        std::set<std::string> extensions (void) const;

        static std::vector<VkLayerProperties> available_layers (void);
    };
}
