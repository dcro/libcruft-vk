/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#include "./command_pool.hpp"

#include "./device.hpp"

using cruft::vk::command_pool;


///////////////////////////////////////////////////////////////////////////////
void
command_pool::reset (const device &dev, VkCommandPoolResetFlags flags)
{
    auto err = vkResetCommandPool (dev.native (), native (), flags);
    error::try_code (err);
}
