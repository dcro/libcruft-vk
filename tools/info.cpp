/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017-2018, Danny Robson <danny@nerdcruft.net>
 */

#include <cruft/util/iterator/infix.hpp>

#include <cruft/vk/load/ostream.hpp>
#include <cruft/vk/load/vtable.hpp>

#include <cruft/vk/instance.hpp>
#include <cruft/vk/physical_device.hpp>
#include <cruft/vk/ostream.hpp>

#include <iostream>


#include "../load/vendor.hpp"
#include "load/vtable.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main (int, char**)
{
    cruft::vk::instance instance;

    std::cout << "[ "
        << "available_layers: [ "
            << cruft::iterator::make_infix (cruft::vk::instance::available_layers ())
        << " ] }"
        << ", instance: " << instance
        << ", devices: [ " << cruft::iterator::make_infix (cruft::vk::physical_device::find (instance)) << " ], "
        << " } ]\n";
}