/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_TRAITS_HPP
#define CRUFT_VK_TRAITS_HPP

#include "fwd.hpp"
#include "vk.hpp"

#include <cruft/util/variadic.hpp>

#include <tuple>
#include <type_traits>


namespace cruft::vk {
    ///////////////////////////////////////////////////////////////////////////
    /// describes the native type that corresponds to a given vk-cruft type.
    template <typename> struct native_traits { };


    //-------------------------------------------------------------------------
    template <> struct native_traits<buffer>            { using type = VkBuffer; };
    template <> struct native_traits<buffer_view>       { using type = VkBufferView; };
    template <> struct native_traits<command_buffer>    { using type = VkCommandBuffer; };
    template <> struct native_traits<command_pool>      { using type = VkCommandPool; };
    template <> struct native_traits<device_memory>     { using type = VkDeviceMemory; };
    template <> struct native_traits<device>            { using type = VkDevice; };
    template <> struct native_traits<event>             { using type = VkEvent; };
    template <> struct native_traits<fence>             { using type = VkFence; };
    template <> struct native_traits<framebuffer>       { using type = VkFramebuffer; };
    template <> struct native_traits<image_view>        { using type = VkImageView; };
    template <> struct native_traits<instance>          { using type = VkInstance; };
    template <> struct native_traits<physical_device>   { using type = VkPhysicalDevice; };
    template <> struct native_traits<pipeline_cache>    { using type = VkPipelineCache; };
    template <> struct native_traits<pipeline_layout>   { using type = VkPipelineLayout; };

    template <> struct native_traits<queue>             { using type = VkQueue; };
    template <> struct native_traits<render_pass>       { using type = VkRenderPass; };
    template <> struct native_traits<semaphore>         { using type = VkSemaphore; };
    template <> struct native_traits<shader_module>     { using type = VkShaderModule; };
    template <> struct native_traits<surface>           { using type = VkSurfaceKHR; };
    template <> struct native_traits<swapchain>         { using type = VkSwapchainKHR; };
    template <> struct native_traits<debug_report> { using type = VkDebugReportCallbackEXT; };

    //-------------------------------------------------------------------------
    template <>
    struct native_traits<pipeline<bindpoint::GRAPHICS>>
    { using type = VkPipeline; };

    template <>
    struct native_traits<pipeline<bindpoint::COMPUTE>>
    { using type = VkPipeline; };


    //-------------------------------------------------------------------------
    template <typename T>
    using native_t = typename native_traits<T>::type;


    ///////////////////////////////////////////////////////////////////////////
    /// defines whether a type is a native vulkan object.
    ///
    /// note that this only returns true for handle types, not parameter types.
    /// eg, VkDevice will return true, but VkDeviceCreateInfo will not.
    template <typename>
    struct is_native : public std::false_type { };


    //-------------------------------------------------------------------------
    #define DEFINE_IS_HANDLE(KLASS) \
    template <>                     \
    struct is_native<KLASS> :       \
         public std::true_type      \
    { };

    VK_NATIVE_TYPE_MAP (DEFINE_IS_HANDLE)
    #undef DEFINE_IS_HANDLE


    //-------------------------------------------------------------------------
    template <typename T>
    static constexpr auto is_native_v = is_native<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    template <typename>
    struct is_wrapper : public std::false_type { };


    //-------------------------------------------------------------------------
    #define IS_WRAPPER(KLASS)   \
        template <>                 \
        struct is_wrapper<KLASS> :  \
            public std::true_type   \
        { };

    VK_TYPE_MAP(IS_WRAPPER)
    #undef IS_WRAPPER


    //-------------------------------------------------------------------------
    template <typename T>
    struct is_wrapper<owned_ptr<T>> :
        public is_wrapper<T>
    { };


    //-------------------------------------------------------------------------
    template <typename T>
    static constexpr auto is_wrapper_v = is_wrapper<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// determines whether a function conceptually returns a singular Vulkan
    /// data structure.
    ///
    /// that is: is the last parameter a pointer to a Vulkan data type?
    ///
    /// a result of true probably implies the function type can be passed to
    /// error::try_query
    template <typename FunctionT>
    struct is_query_function : public std::false_type {};


    //-------------------------------------------------------------------------
    namespace detail {
        template <typename ...Args>
        struct is_query_args : public std::conditional_t<
            std::is_pointer_v<
                std::tuple_element_t<
                    sizeof...(Args) - 1,
                    std::tuple<Args...>
                >
            > && is_native_v<
                std::remove_pointer_t<
                    std::tuple_element_t<
                        sizeof...(Args) - 1,
                        std::tuple<Args...>
                    >
                >
            >,
            std::true_type,
            std::false_type
        > { };
    }


    //-------------------------------------------------------------------------
    template <typename ReturnT, typename ...Args>
    struct is_query_function<
        ReturnT(Args...)noexcept
    > : detail::is_query_args<Args...> { };


    template <typename ReturnT, typename ...Args>
    struct is_query_function<
        ReturnT(*)(Args...)noexcept
    > : detail::is_query_args<Args...> { };


    template <typename ReturnT, typename ...Args>
    struct is_query_function<
        ReturnT(&)(Args...)noexcept
    > : detail::is_query_args<Args...> { };


    //-------------------------------------------------------------------------
    template <typename FunctionT>
    constexpr auto is_query_function_v = is_query_function<FunctionT>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// determines whether a function conceptually returns an array of values.
    ///
    /// that is: does it take/return uint32_t and Vulkan types as the last two
    /// pointer parameters.
    ///
    /// a result of true likely implies such a function pointer can be passed
    /// to error::try_values
    template <typename FunctionT>
    struct is_values_function : public std::false_type {};


    //-------------------------------------------------------------------------
    namespace detail {
        template <typename ...Args>
        struct is_values_args : public std::conditional_t<
            std::is_same_v<
                uint32_t*,
                std::tuple_element_t<
                    sizeof...(Args) - 2,
                    std::tuple<Args...>
                >
            > && std::is_pointer_v<
                std::tuple_element_t<
                    sizeof...(Args) - 1,
                    std::tuple<Args...>
                >
            > && is_native_v<
                std::remove_pointer_t<
                    std::tuple_element_t<
                        sizeof...(Args) - 1,
                        std::tuple<Args...>
                    >
                >
            >,
            std::true_type,
            std::false_type
        > { };
    };

    //-------------------------------------------------------------------------
    template <typename ReturnT, typename ...Args>
    struct is_values_function<
        ReturnT(Args...) noexcept
    > : detail::is_values_args<Args...> { };


    //-------------------------------------------------------------------------
    template <typename ReturnT, typename ...Args>
    struct is_values_function<
        ReturnT(&)(Args...) noexcept
    > : detail::is_values_args<Args...> { };


    //-------------------------------------------------------------------------
    template <typename ReturnT, typename ...Args>
    struct is_values_function<
        ReturnT(*)(Args...) noexcept
    > : detail::is_values_args<Args...> { };


    //-------------------------------------------------------------------------
    template <typename FunctionT>
    constexpr auto is_values_function_v = is_values_function<FunctionT>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// return the native value for the wrapper object
    template <typename SelfT>
    constexpr auto
    native (const object<SelfT> &obj)
    {
        return obj.native ();
    }


    ///------------------------------------------------------------------------
    /// return the native handle as is; supplied for implementation convenience
    /// in other higher order methods.
    template <
        typename NativeT,
        typename = std::enable_if_t<is_native_v<NativeT>, void>
    >
    constexpr auto
    native (NativeT val)
    {
        return val;
    }


    ///------------------------------------------------------------------------
    /// return the underlying native value by querying the data object (not the
    /// owner object).
    template <typename SelfT>
    constexpr auto
    native (const owned_ptr<SelfT> &ptr)
    {
        return native (ptr.get ());
    }


    ///------------------------------------------------------------------------
    /// try to return the vulkan native type if there is one, else return the
    /// value we were given.
    template <typename T, typename DecayT = std::decay_t<T>>
    constexpr auto
    maybe_native (T &&t)
    {
        if constexpr (is_wrapper_v<DecayT>)
            return native (t);
        else if constexpr (is_native_v<DecayT>)
            return native (t);
        else
            return t;
    }


    ///////////////////////////////////////////////////////////////////////////
    /// describes the corresponding value for sType in native structures
    template <typename>
    struct structure_type {};


    //-------------------------------------------------------------------------
    #define DEFINE_STRUCTURE_TYPE(KLASS,VALUE)  \
    template <>                                 \
    struct structure_type<KLASS> :              \
        public std::integral_constant<          \
            VkStructureType,                    \
            PASTE(VK_STRUCTURE_TYPE_,VALUE)     \
        >                                       \
    { }

    DEFINE_STRUCTURE_TYPE (VkInstanceCreateInfo,     INSTANCE_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkApplicationInfo,        APPLICATION_INFO);
    DEFINE_STRUCTURE_TYPE (VkDeviceQueueCreateInfo,  DEVICE_QUEUE_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkDeviceCreateInfo,       DEVICE_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkSwapchainCreateInfoKHR, SWAPCHAIN_CREATE_INFO_KHR);
    DEFINE_STRUCTURE_TYPE (VkImageViewCreateInfo,    IMAGE_VIEW_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkShaderModuleCreateInfo, SHADER_MODULE_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkPipelineShaderStageCreateInfo, PIPELINE_SHADER_STAGE_CREATE_INFO);
    DEFINE_STRUCTURE_TYPE (VkDebugReportCallbackCreateInfoEXT,
                           DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT);

    #undef DEFINE_STRUCTURE_TYPE


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr auto structure_type_v = structure_type<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// describes the native type that owns a given native type, and hence
    /// forms part of the create/destroy process.
    ///
    /// undefined for types that aren't `owned' types.
    template <typename> struct owner_traits {};

    template <> struct owner_traits<buffer> { using type = device; };
    template <> struct owner_traits<command_pool> { using type = device; };
    template <> struct owner_traits<device_memory> { using type = device; };
    template <> struct owner_traits<framebuffer> { using type = device; };
    template <> struct owner_traits<image_view> { using type = device; };
    template <> struct owner_traits<pipeline_layout> { using type = device; };
    template <> struct owner_traits<queue> { using type = device; };
    template <> struct owner_traits<render_pass> { using type = device; };
    template <> struct owner_traits<semaphore> { using type = device; };
    template <> struct owner_traits<shader_module> { using type = device; };
    template <> struct owner_traits<surface> { using type = instance; };
    template <> struct owner_traits<swapchain> { using type = device; };
    template <> struct owner_traits<pipeline<bindpoint::GRAPHICS>> { using type = device; };
    template <> struct owner_traits<pipeline<bindpoint::COMPUTE>> { using type = device; };
    template <> struct owner_traits<debug_report> { using type = instance; };

    template <typename T>
    using owner_t = typename owner_traits<T>::type;


    ///////////////////////////////////////////////////////////////////////////
    /// describes the parameter struct used to create a given vulkan type.
    ///
    /// explicitly does not operate on vk-cruft types, only native types.
    template <typename>
    struct create_info { };


    //-------------------------------------------------------------------------
    #define DEFINE_CREATE_INFO(TARGET,INFO) \
        template <> \
        struct create_info<TARGET> \
        { using type = INFO; }

    DEFINE_CREATE_INFO (VkImageViewCreateInfo, VkImageViewCreateInfo);
    DEFINE_CREATE_INFO (VkShaderModule,        VkShaderModuleCreateInfo);
    DEFINE_CREATE_INFO (VkPipelineLayout,      VkPipelineLayoutCreateInfo);

    #undef DEFINE_CREATE_INFO


    //-------------------------------------------------------------------------
    template <typename T>
    using create_info_t = typename create_info<T>::type;


    ///////////////////////////////////////////////////////////////////////////
    /// lists the `create' and `destroy' methods for a given native type.
    ///
    /// XXX: if such a function does not exist anywhere then we currently use
    /// tuple::ignore to simplify implementation elsewhere. we should probably
    /// investigate std::is_detected for these cases though.
    ///
    /// clang#18781: we make use of references types extensively here to
    /// workaround a potential ODR bug in clang which results in undefined
    /// references.
    /// or potentially i'm being too clever for my own good, but it works fine
    /// in gcc...
    template <typename>
    struct life_traits { };


    //-------------------------------------------------------------------------
    template <> struct life_traits<VkInstance> {
        static constexpr auto& create  = vkCreateInstance;
        static constexpr auto& destroy = vkDestroyInstance;
    };

    template <> struct life_traits<VkDevice> {
        static constexpr auto& create = vkCreateDevice;
        static constexpr auto& destroy = vkDestroyDevice;
    };

    template <> struct life_traits<VkImageView> {
        static constexpr auto& create = vkCreateImageView;
        static constexpr auto& destroy = vkDestroyImageView;
    };

    template <> struct life_traits<VkCommandPool> {
        static constexpr auto& create = vkCreateCommandPool;
        static constexpr auto& destroy = vkDestroyCommandPool;
    };

    template <> struct life_traits<VkFence> {
        static constexpr auto& create = vkCreateFence;
        static constexpr auto& destroy = vkDestroyFence;
    };

    template <> struct life_traits<VkSemaphore> {
        static constexpr auto& create = vkCreateSemaphore;
        static constexpr auto& destroy = vkDestroySemaphore;
    };

    template <> struct life_traits<VkEvent> {
        static constexpr auto& create = vkCreateEvent;
        static constexpr auto& destroy = vkDestroyEvent;
    };

    template <> struct life_traits<VkRenderPass> {
        static constexpr auto& create = vkCreateRenderPass;
        static constexpr auto& destroy = vkDestroyRenderPass;
    };

    template <> struct life_traits<VkFramebuffer> {
        static constexpr auto& create = vkCreateFramebuffer;
        static constexpr auto& destroy = vkDestroyFramebuffer;
    };

    template <> struct life_traits<VkShaderModule> {
        static constexpr auto& create = vkCreateShaderModule;
        static constexpr auto& destroy = vkDestroyShaderModule;
    };

    template <> struct life_traits<VkSurfaceKHR> {
        static constexpr auto& destroy = vkDestroySurfaceKHR;
    };

    template <>
    struct life_traits<VkQueue> {
        static constexpr auto& create = vkGetDeviceQueue;

        static constexpr auto destroy = ::cruft::variadic::ignore<
            native_t<owner_t<queue>>,
            native_t<queue>,
            const VkAllocationCallbacks*
        >;
    };

    template <> struct life_traits<VkPipelineLayout> {
        static constexpr auto& create = vkCreatePipelineLayout;
        static constexpr auto& destroy = vkDestroyPipelineLayout;
    };

    template <> struct life_traits<VkPipelineCache> {
        static constexpr auto& create = vkCreatePipelineCache;
        static constexpr auto& destroy = vkDestroyPipelineCache;
    };

    template <> struct life_traits<VkDeviceMemory> {
        static constexpr auto& create = vkAllocateMemory;
        static constexpr auto& destroy = vkFreeMemory;
    };

    template <> struct life_traits<VkBuffer> {
        static constexpr auto& create = vkCreateBuffer;
        static constexpr auto& destroy = vkDestroyBuffer;
    };

    template <> struct life_traits<VkBufferView> {
        static constexpr auto& create = vkCreateBufferView;
    };


    template <> struct life_traits<VkSwapchainKHR> {
        static constexpr auto& create = vkCreateSwapchainKHR;
        static constexpr auto& destroy = vkDestroySwapchainKHR;
    };


    template <> struct life_traits<VkDebugReportCallbackEXT> {
        static constexpr auto& create = vkCreateDebugReportCallbackEXT;
        static constexpr auto& destroy = vkDestroyDebugReportCallbackEXT;
    };


    // loads all extension function pointers from an instance into the
    // appropriate life_traits pointers where available.
    //
    // this is unambigiously the wrong way of doing things: each instance may
    // have their own function pointers to each of the extension methods.
    //
    // but... we only support one instance for the time being, and this easier.
    void load_traits (instance&);


    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    struct wrapper_traits : public life_traits<native_t<T>> { };


    //-------------------------------------------------------------------------
    template <>
    struct wrapper_traits<pipeline<bindpoint::GRAPHICS>> {
        static constexpr auto &create = vkCreateGraphicsPipelines;
        static constexpr auto &destroy = vkDestroyPipeline;
    };


    //-------------------------------------------------------------------------
    template <>
    struct wrapper_traits<pipeline<bindpoint::COMPUTE>> {
        static constexpr auto &create = vkCreateComputePipelines;
        static constexpr auto &destroy = vkDestroyPipeline;
    };


    ///////////////////////////////////////////////////////////////////////////
    /// describes the functions required to enumerate native types
    template <typename> struct enum_traits { };


    template <> struct enum_traits<VkPhysicalDevice> {
        static constexpr auto &enumerate = vkEnumeratePhysicalDevices;
    };
}

#endif
