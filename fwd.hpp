/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_FWD_HPP
#define CRUFT_VK_FWD_HPP

#include "vk.hpp"

#include <cruft/util/preprocessor.hpp>


namespace cruft::vk {
    template <typename SelfT> struct object;
    template <typename SelfT> struct descendant;
    template <typename SelfT, typename ParentT> struct enumerated;
    template <typename SelfT, typename OwnerT> struct owned;

    template <typename SelfT> class owned_ptr;

    enum class bindpoint {
        GRAPHICS = VK_PIPELINE_BIND_POINT_GRAPHICS,
        COMPUTE  = VK_PIPELINE_BIND_POINT_COMPUTE,
    };

    struct buffer;
    struct buffer_view;
    struct command_buffer;
    struct command_pool;
    struct device;
    struct device_memory;
    struct debug_report;
    struct event;
    struct fence;
    struct framebuffer;
    struct instance;
    struct image;
    struct image_view;
    struct physical_device;
    template <bindpoint> struct pipeline;
    struct pipeline_cache;
    struct pipeline_layout;
    struct queue;
    struct render_pass;
    struct semaphore;
    struct shader_module;
    struct surface;
    struct swapchain;

    class error;
    template <VkResult> class error_code;

    #define VK_BINDPOINT_MAP(FUNC) MAP0(FUNC, bindpoint::GRAPHICS, bindpoint::COMPUTE)

    #define VK_ENUMERATED_TYPE_MAP(FUNC) \
        MAP0(FUNC,               \
             physical_device)

    #define VK_DESCENDANT_TYPE_MAP(FUNC) \
        MAP0(FUNC,       \
             device)

    #define VK_ARRAY_TYPE_MAP(FUNC)                      \
        MAP0(FUNC,                                       \
             pipeline<::cruft::vk::bindpoint::GRAPHICS>, \
             pipeline<::cruft::vk::bindpoint::COMPUTE>   \
        )

    #define VK_OWNED_TYPE_MAP(FUNC) \
        MAP0(FUNC, \
             buffer, \
             command_pool, \
             debug_report, \
             device_memory, \
             framebuffer, \
             image_view, \
             pipeline_layout, \
             queue, \
             render_pass, \
             semaphore, \
             shader_module, \
             surface, \
             swapchain) \
        VK_ARRAY_TYPE_MAP(FUNC)

    #define VK_TYPE_MAP(FUNC) \
        MAP0(FUNC,instance) \
        VK_ENUMERATED_TYPE_MAP(FUNC) \
        VK_DESCENDANT_TYPE_MAP(FUNC) \
        VK_OWNED_TYPE_MAP(FUNC)

    #define VK_NATIVE_ENUM_MAP(FUNC) \
        MAP0(FUNC, \
            VkImageAspectFlagBits, \
            VkShaderStageFlagBits, \
            VkSystemAllocationScope, \
            VkInternalAllocationType, \
            VkQueueFlagBits, \
            VkStructureType, \
            VkFilter, \
            VkSamplerMipmapMode, \
            VkSamplerAddressMode, \
            VkCompareOp, \
            VkBorderColor, \
            VkIndexType, \
            VkBufferCreateFlagBits, \
            VkBufferUsageFlagBits, \
            VkExternalMemoryHandleTypeFlagBitsKHR, \
            VkPrimitiveTopology, \
            VkIndirectCommandsTokenTypeNVX, \
            VkDisplayPlaneAlphaFlagBitsKHR, \
            VkSurfaceTransformFlagBitsKHR, \
            VkCompositeAlphaFlagBitsKHR, \
            VkImageUsageFlagBits, \
            VkViewportCoordinateSwizzleNV, \
            VkPipelineBindPoint, \
            VkExternalMemoryFeatureFlagBitsNV, \
            VkDebugReportFlagBitsEXT, \
            VkDebugReportObjectTypeEXT, \
            VkCommandBufferResetFlagBits, \
            VkPhysicalDeviceType, \
            VkFenceImportFlagBitsKHR, \
            VkExternalFenceHandleTypeFlagBitsKHR, \
            VkQueryType, \
            VkQueryPipelineStatisticFlagBits, \
            VkExternalSemaphoreHandleTypeFlagBitsKHR, \
            VkImageLayout, \
            VkFormatFeatureFlagBits, \
            VkObjectEntryTypeNVX, \
            VkObjectEntryUsageFlagBitsNVX, \
            VkDescriptorType, \
            VkSparseImageFormatFlagBits, \
            VkComponentSwizzle, \
            VkDescriptorSetLayoutCreateFlagBits, \
            VkIndirectCommandsLayoutUsageFlagBitsNVX, \
            VkAttachmentDescriptionFlagBits, \
            VkSampleCountFlagBits, \
            VkAttachmentLoadOp, \
            VkAttachmentStoreOp, \
            VkImageType, \
            VkImageTiling, \
            VkExternalMemoryFeatureFlagBitsKHR, \
            VkStencilFaceFlagBits, \
            VkStencilOp, \
            VkPolygonMode, \
            VkCullModeFlagBits, \
            VkFrontFace, \
            VkImageCreateFlagBits, \
            VkExternalMemoryHandleTypeFlagBitsNV, \
            VkImageViewType, \
            VkSharingMode, \
            VkDynamicState, \
            VkCommandPoolCreateFlagBits, \
            VkSemaphoreImportFlagBitsKHR, \
            VkPipelineStageFlagBits, \
            VkSwapchainCreateFlagBitsKHR, \
            VkMemoryHeapFlagBits, \
            VkPipelineCreateFlagBits, \
            VkBlendOverlapEXT, \
            VkSurfaceCounterFlagBitsEXT, \
            VkDescriptorUpdateTemplateTypeKHR, \
            VkExternalFenceFeatureFlagBitsKHR, \
            VkQueryResultFlagBits, \
            VkSparseMemoryBindFlagBits, \
            VkValidationCacheHeaderVersionEXT, \
            VkColorSpaceKHR, \
            VkPresentModeKHR, \
            VkCommandPoolResetFlagBits, \
            VkVertexInputRate, \
            VkSubpassDescriptionFlagBits, \
            VkAccessFlagBits, \
            VkDependencyFlagBits, \
            VkExternalSemaphoreFeatureFlagBitsKHR, \
            VkBlendFactor, \
            VkBlendOp, \
            VkColorComponentFlagBits, \
            VkMemoryPropertyFlagBits, \
            VkCommandBufferUsageFlagBits, \
            VkQueryControlFlagBits, \
            VkCommandBufferLevel, \
            VkValidationCheckEXT, \
            VkSubpassContents, \
            VkLogicOp, \
            VkDisplayPowerStateEXT, \
            VkFenceCreateFlagBits, \
            VkDisplayEventTypeEXT, \
            VkDescriptorPoolCreateFlagBits, \
            VkCoverageModulationModeNV, \
            VkObjectType, \
            VkPipelineCacheHeaderVersion, \
            VkDiscardRectangleModeEXT, \
            VkDeviceEventTypeEXT, \
            VkSamplerReductionModeEXT, \
            VkRasterizationOrderAMD \
        )

    #define VK_NATIVE_STRUCT_MAP(FUNC) \
        MAP0(FUNC, \
             VkClearColorValue, \
             VkClearValue, \
             VkOffset2D, \
             VkOffset3D, \
             VkExtent2D, \
             VkExtent3D, \
             VkViewport, \
             VkRect2D, \
             VkClearRect, \
             VkComponentMapping, \
             VkPhysicalDeviceProperties, \
             VkExtensionProperties, \
             VkLayerProperties, \
             VkApplicationInfo, \
             VkAllocationCallbacks, \
             VkDeviceQueueCreateInfo, \
             VkDeviceCreateInfo, \
             VkInstanceCreateInfo, \
             VkQueueFamilyProperties, \
             VkPhysicalDeviceMemoryProperties, \
             VkMemoryAllocateInfo, \
             VkMemoryRequirements, \
             VkSparseImageFormatProperties, \
             VkSparseImageMemoryRequirements, \
             VkMemoryType, \
             VkMemoryHeap, \
             VkMappedMemoryRange, \
             VkFormatProperties, \
             VkImageFormatProperties, \
             VkDescriptorBufferInfo, \
             VkDescriptorImageInfo, \
             VkWriteDescriptorSet, \
             VkCopyDescriptorSet, \
             VkBufferCreateInfo, \
             VkBufferViewCreateInfo, \
             VkImageSubresource, \
             VkImageSubresourceLayers, \
             VkImageSubresourceRange, \
             VkMemoryBarrier, \
             VkBufferMemoryBarrier, \
             VkImageMemoryBarrier, \
             VkImageCreateInfo, \
             VkSubresourceLayout, \
             VkImageViewCreateInfo, \
             VkBufferCopy, \
             VkSparseMemoryBind, \
             VkSparseImageMemoryBind, \
             VkSparseBufferMemoryBindInfo, \
             VkSparseImageOpaqueMemoryBindInfo, \
             VkSparseImageMemoryBindInfo, \
             VkBindSparseInfo, \
             VkImageCopy, \
             VkImageBlit, \
             VkBufferImageCopy, \
             VkImageResolve, \
             VkShaderModuleCreateInfo, \
             VkDescriptorSetLayoutBinding, \
             VkDescriptorSetLayoutCreateInfo, \
             VkDescriptorPoolSize, \
             VkDescriptorPoolCreateInfo, \
             VkDescriptorSetAllocateInfo, \
             VkSpecializationMapEntry, \
             VkSpecializationInfo, \
             VkPipelineShaderStageCreateInfo, \
             VkComputePipelineCreateInfo, \
             VkVertexInputBindingDescription, \
             VkVertexInputAttributeDescription, \
             VkPipelineVertexInputStateCreateInfo, \
             VkPipelineInputAssemblyStateCreateInfo, \
             VkPipelineTessellationStateCreateInfo, \
             VkPipelineViewportStateCreateInfo, \
             VkPipelineRasterizationStateCreateInfo, \
             VkPipelineMultisampleStateCreateInfo, \
             VkPipelineColorBlendAttachmentState, \
             VkPipelineColorBlendStateCreateInfo, \
             VkPipelineDynamicStateCreateInfo, \
             VkStencilOpState, \
             VkPipelineDepthStencilStateCreateInfo, \
             VkGraphicsPipelineCreateInfo, \
             VkPipelineCacheCreateInfo, \
             VkPushConstantRange, \
             VkPipelineLayoutCreateInfo, \
             VkSamplerCreateInfo, \
             VkCommandPoolCreateInfo, \
             VkCommandBufferAllocateInfo, \
             VkCommandBufferInheritanceInfo, \
             VkCommandBufferBeginInfo, \
             VkRenderPassBeginInfo, \
             VkClearDepthStencilValue, \
             VkClearAttachment, \
             VkAttachmentDescription, \
             VkAttachmentReference, \
             VkSubpassDescription, \
             VkSubpassDependency, \
             VkRenderPassCreateInfo, \
             VkEventCreateInfo, \
             VkFenceCreateInfo, \
             VkPhysicalDeviceFeatures, \
             VkPhysicalDeviceSparseProperties, \
             VkPhysicalDeviceLimits, \
             VkSemaphoreCreateInfo, \
             VkQueryPoolCreateInfo, \
             VkFramebufferCreateInfo, \
             VkDrawIndirectCommand, \
             VkDrawIndexedIndirectCommand, \
             VkDispatchIndirectCommand, \
             VkSubmitInfo, \
             VkDisplayPropertiesKHR, \
             VkDisplayPlanePropertiesKHR, \
             VkDisplayModeParametersKHR, \
             VkDisplayModePropertiesKHR, \
             VkDisplayModeCreateInfoKHR, \
             VkDisplayPlaneCapabilitiesKHR, \
             VkDisplaySurfaceCreateInfoKHR, \
             VkDisplayPresentInfoKHR, \
             VkSurfaceCapabilitiesKHR, \
             /*VkXcbSurfaceCreateInfoKHR,*/ \
             VkSurfaceFormatKHR, \
             VkSwapchainCreateInfoKHR, \
             VkPresentInfoKHR, \
             VkDebugReportCallbackCreateInfoEXT, \
             VkValidationFlagsEXT, \
             VkPipelineRasterizationStateRasterizationOrderAMD, \
             VkDebugMarkerObjectNameInfoEXT, \
             VkDebugMarkerObjectTagInfoEXT, \
             VkDebugMarkerMarkerInfoEXT, \
             VkDedicatedAllocationImageCreateInfoNV, \
             VkDedicatedAllocationBufferCreateInfoNV, \
             VkDedicatedAllocationMemoryAllocateInfoNV, \
             VkExternalImageFormatPropertiesNV, \
             VkExternalMemoryImageCreateInfoNV, \
             VkExportMemoryAllocateInfoNV, \
             VkDeviceGeneratedCommandsFeaturesNVX, \
             VkDeviceGeneratedCommandsLimitsNVX, \
             VkIndirectCommandsTokenNVX, \
             VkIndirectCommandsLayoutTokenNVX, \
             VkIndirectCommandsLayoutCreateInfoNVX, \
             VkCmdProcessCommandsInfoNVX, \
             VkCmdReserveSpaceForCommandsInfoNVX, \
             VkObjectTableCreateInfoNVX, \
             VkObjectTableEntryNVX, \
             VkObjectTablePipelineEntryNVX, \
             VkObjectTableDescriptorSetEntryNVX, \
             VkObjectTableVertexBufferEntryNVX, \
             VkObjectTableIndexBufferEntryNVX, \
             VkObjectTablePushConstantEntryNVX, \
             VkPhysicalDeviceFeatures2KHR, \
             VkPhysicalDeviceProperties2KHR, \
             VkFormatProperties2KHR, \
             VkImageFormatProperties2KHR, \
             VkPhysicalDeviceImageFormatInfo2KHR, \
             VkQueueFamilyProperties2KHR, \
             VkPhysicalDeviceMemoryProperties2KHR, \
             VkSparseImageFormatProperties2KHR, \
             VkPhysicalDeviceSparseImageFormatInfo2KHR, \
             VkPhysicalDevicePushDescriptorPropertiesKHR, \
             VkPresentRegionsKHR, \
             VkPresentRegionKHR, \
             VkRectLayerKHR, \
             VkPhysicalDeviceVariablePointerFeaturesKHR, \
             VkExternalMemoryPropertiesKHR, \
             VkPhysicalDeviceExternalImageFormatInfoKHR, \
             VkExternalImageFormatPropertiesKHR, \
             VkPhysicalDeviceExternalBufferInfoKHR, \
             VkExternalBufferPropertiesKHR, \
             VkPhysicalDeviceIDPropertiesKHR, \
             VkExternalMemoryImageCreateInfoKHR, \
             VkExternalMemoryBufferCreateInfoKHR, \
             VkExportMemoryAllocateInfoKHR, \
             VkImportMemoryFdInfoKHR, \
             VkMemoryFdPropertiesKHR, \
             VkMemoryGetFdInfoKHR, \
             VkPhysicalDeviceExternalSemaphoreInfoKHR, \
             VkExternalSemaphorePropertiesKHR, \
             VkExportSemaphoreCreateInfoKHR, \
             VkImportSemaphoreFdInfoKHR, \
             VkSemaphoreGetFdInfoKHR, \
             VkPhysicalDeviceExternalFenceInfoKHR, \
             VkExternalFencePropertiesKHR, \
             VkExportFenceCreateInfoKHR, \
             VkImportFenceFdInfoKHR, \
             VkFenceGetFdInfoKHR, \
             VkSurfaceCapabilities2EXT, \
             VkDisplayPowerInfoEXT, \
             VkDeviceEventInfoEXT, \
             VkDisplayEventInfoEXT, \
             VkSwapchainCounterCreateInfoEXT, \
             VkDescriptorUpdateTemplateCreateInfoKHR, \
             VkXYColorEXT, \
             VkHdrMetadataEXT, \
             VkRefreshCycleDurationGOOGLE, \
             VkPastPresentationTimingGOOGLE, \
             VkPresentTimesInfoGOOGLE, \
             VkPresentTimeGOOGLE, \
             VkViewportWScalingNV, \
             VkPipelineViewportWScalingStateCreateInfoNV, \
             VkViewportSwizzleNV, \
             VkPipelineViewportSwizzleStateCreateInfoNV, \
             VkPhysicalDeviceDiscardRectanglePropertiesEXT, \
             VkPipelineDiscardRectangleStateCreateInfoEXT, \
             VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX, \
             VkPhysicalDeviceSurfaceInfo2KHR, \
             VkSurfaceCapabilities2KHR, \
             VkSurfaceFormat2KHR, \
             VkSharedPresentSurfaceCapabilitiesKHR, \
             VkPhysicalDevice16BitStorageFeaturesKHR, \
             VkBufferMemoryRequirementsInfo2KHR, \
             VkImageMemoryRequirementsInfo2KHR, \
             VkImageSparseMemoryRequirementsInfo2KHR, \
             VkMemoryRequirements2KHR, \
             VkSparseImageMemoryRequirements2KHR, \
             VkMemoryDedicatedRequirementsKHR, \
             VkMemoryDedicatedAllocateInfoKHR, \
             VkTextureLODGatherFormatPropertiesAMD, \
             VkPipelineCoverageToColorStateCreateInfoNV, \
             VkPhysicalDeviceSamplerFilterMinmaxPropertiesEXT, \
             VkSampleLocationEXT, \
             VkSampleLocationsInfoEXT, \
             VkAttachmentSampleLocationsEXT, \
             VkSubpassSampleLocationsEXT, \
             VkRenderPassSampleLocationsBeginInfoEXT, \
             VkPipelineSampleLocationsStateCreateInfoEXT, \
             VkPhysicalDeviceSampleLocationsPropertiesEXT, \
             VkMultisamplePropertiesEXT, \
             VkSamplerReductionModeCreateInfoEXT, \
             VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT, \
             VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT, \
             VkPipelineColorBlendAdvancedStateCreateInfoEXT, \
             VkPipelineCoverageModulationStateCreateInfoNV, \
             VkValidationCacheCreateInfoEXT, \
             VkShaderModuleValidationCacheCreateInfoEXT)

    #define VK_NATIVE_TYPE_MAP(FUNC) \
        VK_NATIVE_ENUM_MAP(FUNC) \
        VK_NATIVE_STRUCT_MAP(FUNC)

}

#endif
