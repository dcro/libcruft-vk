/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_PHYSICAL_DEVICE_HPP
#define CRUFT_VK_PHYSICAL_DEVICE_HPP

#include "./object.hpp"

#include "./vk.hpp"

#include <set>
#include <string>

namespace cruft::vk {
    struct physical_device : public enumerated<physical_device,instance> {
        using enumerated::enumerated;

        std::set<std::string> extensions (void) const;

        VkPhysicalDeviceProperties properties (void) const;
        VkPhysicalDeviceFeatures features (void) const;
        VkPhysicalDeviceMemoryProperties memory_properties (void) const;

        std::vector<VkQueueFamilyProperties> queue_families (void) const;

        std::string name (void) const;
    };
}

#endif
