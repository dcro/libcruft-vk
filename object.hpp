/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_OBJECT_HPP
#define CRUFT_VK_OBJECT_HPP

#include "./traits.hpp"
#include "./except.hpp"

#include <utility>
#include <vector>

namespace cruft::vk {
    /// the base class for all non-trivial vulkan objects requiring lifetime
    /// management.
    template <typename SelfT>
    struct object {
        using native_type = native_t<SelfT>;

        object (const native_t<SelfT> &_native):
            m_native (_native)
        {
            CHECK_NEQ (m_native, static_cast<native_type> (VK_NULL_HANDLE));
        }

        object (object &&rhs):
            m_native (VK_NULL_HANDLE)
        { std::swap (m_native, rhs.m_native); }

        object (const object&) = delete;

        const auto& native (void) const& { return m_native; }
        auto& native (void)& { return m_native; }

    protected:
        native_t<SelfT> m_native;
    };


    ///////////////////////////////////////////////////////////////////////////
    template <typename SelfT>
    struct root : public object<SelfT> {
        template <typename ...Args>
        root (Args &&...args):
            object<SelfT> (
                cruft::vk::error::try_query (
                    wrapper_traits<SelfT>::create,
                    std::forward<Args> (args)...
                )
            )
        { ; }

        ~root ()
        {
            wrapper_traits<SelfT>::destroy (this->native (), nullptr);
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    /// a vulkan object that must be directly instantiated through
    /// constructor arguments, rather than being enumerated and owned by
    /// another object.
    template <typename SelfT>
    struct descendant : public object<SelfT> {
        template <typename ParentT, typename ...Args>
        descendant (ParentT &parent, Args &&...args):
            object<SelfT> (
                cruft::vk::error::try_query(
                    wrapper_traits<SelfT>::create,
                    parent.native (),
                    std::forward<Args> (args)...
                )
            )
        { ; }


        //---------------------------------------------------------------------
        ~descendant ()
        {
            wrapper_traits<SelfT>::destroy (this->native (), nullptr);
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    /// A vulkan object that is obtained by listings from a parent object.
    template <typename SelfT, typename ParentT>
    struct enumerated : public object<SelfT> {
        using object<SelfT>::object;

        /// Returns a vector of available objects given a parent object.
        static std::vector<SelfT>
        find (const ParentT &parent)
        {
            return error::try_handles<SelfT> (
                enum_traits<native_t<SelfT>>::enumerate,
                parent.native ()
            );
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    /// a vulkan object that is directly owned by a parent object.
    ///
    /// typically implies that this object must be freed using a reference to
    /// the parent object.
    template <typename SelfT, typename OwnerT>
    struct owned : public object<SelfT> {
        using owner_t = OwnerT;

        using object<SelfT>::object;

        ///////////////////////////////////////////////////////////////////////
        template <typename ...Args>
        owned (OwnerT &owner, Args &&...args):
            object<SelfT> (
                error::try_query (
                    wrapper_traits<SelfT>::create,
                    owner.native (),
                    std::forward<Args> (args)...
                )
            )
        { ; }


        //---------------------------------------------------------------------
        owned (owned &&rhs):
            object<SelfT> (std::move (rhs))
        { ; }


        //---------------------------------------------------------------------
        ~owned ()
        {
            CHECK_EQ (this->native (), static_cast<typename SelfT::native_type> (VK_NULL_HANDLE));
        }


        //---------------------------------------------------------------------
        void
        destroy (OwnerT &owner)
        {
            wrapper_traits<SelfT>::destroy (owner.native (), this->native (), nullptr);
            this->m_native = VK_NULL_HANDLE;
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    template <typename SelfT>
    class owned_ptr {
    public:
        owned_ptr (SelfT &&_self, owner_t<SelfT> &_owner):
            m_self (std::move (_self)),
            m_owner (_owner)
        { ; }

        owned_ptr (owned_ptr &&rhs):
            m_self (std::move (rhs.m_self)),
            m_owner (rhs.m_owner)
        { ; }

        ~owned_ptr ()
        {
            m_self.destroy (m_owner);
        }

        // it's unclear whether we want to work around reseating m_owner,
        // or if this operation just isn't sufficiently prevalent to
        // justify the work.
        owned_ptr& operator= (owned_ptr &&rhs) = delete;

        const SelfT& get (void) const& { return m_self; }
        SelfT& get (void)& { return m_self; }

        SelfT& operator* ()& noexcept
        { return m_self; }

        const SelfT*
        operator-> () const& noexcept
        { return &m_self; }

        SelfT*
        operator-> ()& noexcept
        { return &m_self; }

        auto&
        owner (void)& noexcept
        { return m_owner; }

        const auto&
        owner (void) const& noexcept
        { return m_owner; }

    private:
        SelfT m_self;
        owner_t<SelfT> &m_owner;
    };


    //-------------------------------------------------------------------------
    template <typename SelfT, typename OwnerT>
    auto
    make_owned (native_t<SelfT> _native, OwnerT &_owner)
    {
        return owned_ptr<SelfT> {
            SelfT (_native), _owner
        };
    }


    //-------------------------------------------------------------------------
    template <typename SelfT, typename OwnerT, typename ...Args>
    auto
    make_owned (OwnerT &owner, Args &&...args)
    {
        return owned_ptr<SelfT> {
            SelfT { owner, std::forward<Args> (args)... },
            owner
        };
    };


    ///////////////////////////////////////////////////////////////////////////
    template <typename ReturnT, typename ...Args>
    struct function_wrapper {
        using value_type = ReturnT(*)(Args...) noexcept;

        constexpr function_wrapper (value_type _value): m_function {_value} { ; }
        constexpr function_wrapper (): m_function { nullptr } { ; }


        value_type
        reset (value_type _value)
        {
            value_type old = std::move (m_function);
            m_function = _value;
            return old;
        }


        template <typename SelfT, typename ...Tail>
        auto
        operator () (const owned_ptr<SelfT> &owned, Tail &&...tail) const
        {
            return dispatch (owned.owner (), owned.get (), std::forward<Tail> (tail)...);
        }


        template <typename ...Tail>
        auto operator () (Tail &&...tail) const
        {
            return dispatch (std::forward<Tail> (tail)...);
        }


    private:
        template <typename ...Tail>
        auto dispatch (Tail &&...tail) const
        {
            if constexpr (is_values_function_v<value_type>)
                return error::try_values (m_function, std::forward<Tail> (tail)...);
            else if constexpr (is_query_function_v<value_type>)
                return error::try_query (m_function, std::forward<Tail> (tail)...);
            else
                return error::try_func (m_function, std::forward<Tail> (tail)...);
        }


        value_type m_function;
    };


    #define WRAP_FUNCTION(NAME,FUNC)                                \
    namespace detail { const function_wrapper __##NAME {FUNC}; }    \
    template <typename ...Args>                                     \
    auto NAME (Args &&...args)                                      \
    {                                                               \
        return detail::__##NAME (std::forward<Args> (args)... );    \
    };
};

#endif
