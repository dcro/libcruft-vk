/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_EXCEPT_HPP
#define CRUFT_VK_EXCEPT_HPP

#include "./vk.hpp"

#include <cruft/util/debug/assert.hpp>
#include <cruft/util/types/traits.hpp>

#include <exception>
#include <functional>
#include <type_traits>


namespace cruft::vk {
    class error : public std::exception {
    public:
        static void try_code (VkResult);
        static void throw_code [[noreturn]] (VkResult);


        /// invokes a supplied function with the given arguments and tests
        /// that it indicates success using try_code iff it returns a VkResult,
        /// else it returns the result of the function.
        ///
        /// while it would simplify writing this function to avoid
        /// non-VkResult functions it simplifies some of the upper layers of
        /// try_foo functions to allow them anyway (eg,
        /// vkGetBufferMemoryRequirements is otherwise usable with try_query
        /// if we ignore the VkResult requirement)
        template <
            typename FuncT,
            typename ...Args
        >
        static auto
        try_func (FuncT &&func, Args &&...args)
        {
            constexpr bool returns_vkresult = std::is_same_v<
                typename func_traits<FuncT>::return_type,
                VkResult
            >;

            if constexpr (returns_vkresult) {
                try_code (
                    std::invoke (
                        std::forward<FuncT> (func),
                        maybe_native (std::forward<Args> (args))...
                    )
                );
                return;
            } else {
                return std::invoke (
                    std::forward<FuncT> (func),
                    maybe_native (std::forward<Args> (args))...
                );
            }
        }


        /// returns a ValueT using an invokable FuncT with arguments
        /// [Args..., &ValueT] and returns the result after testing for
        /// success.
        template <typename FuncT, typename ...Args>
        static auto
        try_query (FuncT &&func, Args &&...args)
        {
            using ValueT = std::remove_pointer_t<
                std::tuple_element_t<
                    sizeof... (Args),
                    typename func_traits<FuncT>::argument_types
                >
            >;

            ValueT value;
            try_func (func, std::forward<Args> (args)..., &value);
            return value;
        }


        ///--------------------------------------------------------------------
        /// returns a container of values obtained from FuncT
        ///
        /// FuncT must take arguments of the form:
        ///     ... FuncT (..., uint32_t *length, ResultT *values)
        /// and must return the expected length when values is null.
        ///
        /// the return type will be checked by try_code if it is a VkResult.
        ///
        /// the container will dynamically size so that it can contain all
        /// available values.
        template <
            template <typename...> class ContainerT = std::vector,
            typename FuncT,
            typename ...Args
        >
        static auto
        try_values (FuncT &&func, Args &&...args)
        {
            // extract the number of elements available
            uint32_t expected = 0;
            try_func (func, args..., &expected, nullptr);

            // find the type of the last argument, ie. the values we're
            // requesting
            using ValueT = std::remove_pointer_t<
                std::tuple_element_t<
                    sizeof...(Args) + 1,
                    typename func_traits<FuncT>::argument_types
                >
            >;

            ContainerT<ValueT> values (expected);
            uint32_t found = expected;
            try_func (func, args..., &found, values.data ());
            CHECK_EQ (expected, found);
            return values;
        }


        ///--------------------------------------------------------------------
        /// Safely calls a function that returns an array of Handle objects,
        /// and returns a vector of wrapped objects.
        template <
            typename ReturnT,
            template <typename,typename...> class ContainerT = std::vector,
            typename FuncT,
            typename ...Args
        >
        static auto
        try_handles (FuncT &&func, Args &&...args)
        {
            // extract the number of elements available
            uint32_t expected = 0;
            try_func (func, args..., &expected, nullptr);

            // find the type of the last argument, ie. the values we're
            // requesting
            using ValueT = std::remove_pointer_t<
                std::tuple_element_t<
                    sizeof...(Args) + 1,
                    typename func_traits<FuncT>::argument_types
                >
            >;

            // return the values on the stack temporarily as we may need to
            // convert them to a seperate result type.
            ValueT values[expected];
            uint32_t found = expected;
            try_func (func, args..., &found, &values[0]);
            CHECK_EQ (expected, found);

            // convert to the requested type
            return ContainerT<ReturnT> {
                values + 0,
                values + found,
            };
        }
    };


    //-------------------------------------------------------------------------
    class invalid_argument : public error {
    public:
        invalid_argument (const char *_what) noexcept:
            m_what (_what)
        { ; }

        const char*
        what (void) const noexcept override
        {
            return m_what;
        }

    private:
        const char *m_what;
    };


    //-------------------------------------------------------------------------
    template <VkResult>
    class error_code : public error {
    public:
        const char* what (void) const noexcept override;
    };
}


#endif
