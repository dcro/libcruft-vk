/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./shader_module.hpp"

#include "./device.hpp"

#include <cruft/util/io.hpp>

using cruft::vk::shader_module;


///////////////////////////////////////////////////////////////////////////////
struct cruft::vk::shader_module::cookie : public create_t {
public:
    cookie (std::filesystem::path const &src):
        create_t {},
        m_bytes (cruft::slurp<uint32_t> (src))
    {
        CHECK_MOD (m_bytes.size (), 4);

        sType = cruft::vk::structure_type_v<VkShaderModuleCreateInfo>;
        codeSize = m_bytes.size ();
        pCode = reinterpret_cast<const uint32_t*> (m_bytes.data ());
    }

private:
    std::vector<uint32_t> m_bytes;
};


///////////////////////////////////////////////////////////////////////////////
shader_module::shader_module (
    device &owner,
    std::filesystem::path const &src
):
    shader_module (owner, cookie (src))
{ ; }


//-----------------------------------------------------------------------------
shader_module::shader_module (device &owner, const cookie &info):
    owned (owner, &info, nullptr)
{ ; }
