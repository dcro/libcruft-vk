/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_COMMAND_BUFFER_HPP
#define CRUFT_VK_COMMAND_BUFFER_HPP

#include "./object.hpp"

#include <cruft/util/view.hpp>

namespace cruft::vk {
    struct command_buffer : public owned<command_buffer,device> {
        void reset (VkCommandBufferResetFlags);

        void begin (const VkCommandBufferBeginInfo&);
        void begin (const VkRenderPassBeginInfo&, VkSubpassContents);
        void end (void);
        void end_pass (void);

        template <size_t N>
        void execute (const command_buffer (&buffers)[N]);

        void next_subpass (VkSubpassContents);

        template <bindpoint BindpointV>
        void bind (const pipeline<BindpointV>&);

        void set (const event&, VkPipelineStageFlags);
        void reset (const event&, VkPipelineStageFlags);

        void wait (cruft::view<const event*>,
                   VkPipelineStageFlags src_mask, 
                   VkPipelineStageFlags dst_mask, 
                   cruft::view<const VkMemoryBarrier*>,
                   cruft::view<const VkBufferMemoryBarrier*>,
                   cruft::view<const VkImageMemoryBarrier*>);

        void pipeline_barrier (
                   VkPipelineStageFlags src_mask, 
                   VkPipelineStageFlags dst_mask,
                   VkDependencyFlags,
                   cruft::view<const VkMemoryBarrier*>,
                   cruft::view<const VkBufferMemoryBarrier*>,
                   cruft::view<const VkImageMemoryBarrier*>);
    };
}

#endif
