/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./except.hpp"

#include <cruft/util/debug/panic.hpp>
#include <cruft/util/preprocessor.hpp>

using cruft::vk::error;
using cruft::vk::error_code;


///////////////////////////////////////////////////////////////////////////////
#define VK_RESULT_MAP(FUNC)\
    MAP0(FUNC, \
         VK_SUCCESS, \
         VK_NOT_READY, \
         VK_TIMEOUT, \
         VK_EVENT_SET, \
         VK_EVENT_RESET, \
         VK_INCOMPLETE, \
         VK_ERROR_OUT_OF_HOST_MEMORY, \
         VK_ERROR_OUT_OF_DEVICE_MEMORY, \
         VK_ERROR_INITIALIZATION_FAILED, \
         VK_ERROR_DEVICE_LOST, \
         VK_ERROR_MEMORY_MAP_FAILED, \
         VK_ERROR_LAYER_NOT_PRESENT, \
         VK_ERROR_EXTENSION_NOT_PRESENT, \
         VK_ERROR_FEATURE_NOT_PRESENT, \
         VK_ERROR_INCOMPATIBLE_DRIVER, \
         VK_ERROR_TOO_MANY_OBJECTS, \
         VK_ERROR_FORMAT_NOT_SUPPORTED, \
         VK_ERROR_FRAGMENTED_POOL, \
         VK_ERROR_VALIDATION_FAILED_EXT, \
         VK_ERROR_SURFACE_LOST_KHR, \
         VK_ERROR_NATIVE_WINDOW_IN_USE_KHR, \
         VK_SUBOPTIMAL_KHR, \
         VK_ERROR_OUT_OF_DATE_KHR, \
         VK_ERROR_INCOMPATIBLE_DISPLAY_KHR, \
         VK_ERROR_OUT_OF_POOL_MEMORY_KHR, \
         VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR, \
         VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT, \
         VK_ERROR_INVALID_SHADER_NV, \
         VK_ERROR_FRAGMENTATION_EXT, \
         VK_ERROR_NOT_PERMITTED_EXT, \
         VK_ERROR_INVALID_DEVICE_ADDRESS_EXT)


///////////////////////////////////////////////////////////////////////////////
static constexpr
const char*
to_string (VkResult res)
{
    switch (res) {
    case VK_SUCCESS:    return "success";

    // partial successes
    case VK_NOT_READY:      return "not ready";
    case VK_TIMEOUT:        return "timeout";
    case VK_EVENT_SET:      return "event set";
    case VK_EVENT_RESET:    return "event reset";
    case VK_INCOMPLETE:     return "incomplete";

    // errors
    case VK_ERROR_OUT_OF_HOST_MEMORY:       return "out of host memory";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY:     return "out of device memory";
    case VK_ERROR_INITIALIZATION_FAILED:    return "initialization failed";
    case VK_ERROR_DEVICE_LOST:              return "device lost";
    case VK_ERROR_MEMORY_MAP_FAILED:        return "memory map failed";
    case VK_ERROR_LAYER_NOT_PRESENT:        return "layer not present";
    case VK_ERROR_EXTENSION_NOT_PRESENT:    return "extension not present";
    case VK_ERROR_FEATURE_NOT_PRESENT:      return "feature not present";
    case VK_ERROR_INCOMPATIBLE_DRIVER:      return "incompatible driver";
    case VK_ERROR_TOO_MANY_OBJECTS:         return "too many objects";
    case VK_ERROR_FORMAT_NOT_SUPPORTED:     return "format not supported";
    case VK_ERROR_FRAGMENTED_POOL:          return "fragmented pool";

    // EXT
    case VK_ERROR_VALIDATION_FAILED_EXT:    return "validation failed";
    case VK_ERROR_FRAGMENTATION_EXT:        return "too much fragmentation";
    case VK_ERROR_NOT_PERMITTED_EXT:        return "not permitted";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "invalid DRM format modifier plane layout";
    case VK_ERROR_INVALID_DEVICE_ADDRESS_EXT:   return "invalid device address";

    // KHR
    case VK_ERROR_SURFACE_LOST_KHR:             return "surface lost";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:     return "native window in use";
    case VK_SUBOPTIMAL_KHR:                     return "suboptimal";
    case VK_ERROR_OUT_OF_DATE_KHR:              return "out of date";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:     return "incompatible display";
    case VK_ERROR_OUT_OF_POOL_MEMORY_KHR:       return "out of pool memory";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR:  return "invalid external handle";

    // NV
    case VK_ERROR_INVALID_SHADER_NV: return "invalid shader";
    }

    unreachable ();
}


///////////////////////////////////////////////////////////////////////////////
void
error::try_code (VkResult res)
{
    if (__builtin_expect (res >= 0, true))
        return;

    throw_code (res);
}


//-----------------------------------------------------------------------------
void
error::throw_code (VkResult res)
{
#define THROW_CODE(C) case C: throw error_code<C> ();
    switch (res) {
        VK_RESULT_MAP (THROW_CODE)
    }
#undef THROW_CODE

    unreachable ();
}


///////////////////////////////////////////////////////////////////////////////
template <VkResult res>
const char*
error_code<res>::what (void) const noexcept
{
    return to_string (res);
}


///////////////////////////////////////////////////////////////////////////////
#define ERROR_CODE(R) template class cruft::vk::error_code<R>;
VK_RESULT_MAP(ERROR_CODE)
