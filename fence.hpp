/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_FENCE_HPP
#define CRUFT_VK_FENCE_HPP

#include "./object.hpp"

namespace cruft::vk {
    struct fence : owned<fence,device> {
        bool is_ready (const device &dev);

        static void reset (const device&, fence *first, fence *last);
        static void wait (const device&, fence *first, fence *last, uint64_t timeout);
        static void wait_all (const device&, fence *first, fence *last, uint64_t timeout);
    };
}

#endif
