/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017 Danny Robson <danny@nerdcruft.net>
 */

#include "./device_memory.hpp"

#include "./device.hpp"

using cruft::vk::device_memory;


///////////////////////////////////////////////////////////////////////////////
cruft::vk::map_t::~map_t ()
{
    CHECK_ZERO (cbegin ());
    CHECK_ZERO (cend   ());
}


//-----------------------------------------------------------------------------
cruft::vk::map_t&
cruft::vk::map_t::operator= (std::nullptr_t ptr)
{
    cruft::view<std::byte*>::operator= (ptr);
    return *this;
}


//-----------------------------------------------------------------------------
void
cruft::vk::map_t::destroy (device &_device, device_memory &_memory)
{
    CHECK_NEZ (cbegin ());
    CHECK_NEZ (cend   ());

    vkUnmapMemory (_device.native (), _memory.native ());

    *this = nullptr;
}



///////////////////////////////////////////////////////////////////////////////
cruft::vk::map_t
cruft::vk::map (device &_device, device_memory &_memory, int len)
{
    void *ptr = nullptr;
    auto err = vkMapMemory (_device.native (), _memory.native (), 0, len, 0, &ptr);
    error::try_code (err);

    return {
        reinterpret_cast<std::byte*> (ptr),
        reinterpret_cast<std::byte*> (ptr) + len
    };
}



///////////////////////////////////////////////////////////////////////////////
cruft::vk::owned_map_t::owned_map_t (owned_ptr<device_memory> &_memory,
                                     std::byte *first,
                                     std::byte *last):
    map_t (first, last),
    m_memory (_memory)
{ ; }


//-----------------------------------------------------------------------------
cruft::vk::owned_map_t::~owned_map_t ()
{
    destroy ();
}


//-----------------------------------------------------------------------------
void
cruft::vk::owned_map_t::destroy ()
{
    if (cbegin ())
        map_t::destroy (m_memory.owner (), *m_memory);
}



///////////////////////////////////////////////////////////////////////////////
cruft::vk::owned_map_t
cruft::vk::map (owned_ptr<device_memory> &_memory, int len)
{
    void *ptr = nullptr;
    auto err = vkMapMemory (_memory.owner ().native (), _memory->native (), 0, len, 0, &ptr);
    error::try_code (err);

    return owned_map_t {
        _memory,
        reinterpret_cast<std::byte*> (ptr),
        reinterpret_cast<std::byte*> (ptr) + len
    };
};



///////////////////////////////////////////////////////////////////////////////
VkDeviceSize
device_memory::commitment (const device &dev) const
{
    VkDeviceSize size;
    vkGetDeviceMemoryCommitment (dev.native (), native (), &size);
    return size;
}
