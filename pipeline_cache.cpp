/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./pipeline_cache.hpp"

#include "./device.hpp"

#include <cruft/util/cast.hpp>

using cruft::vk::pipeline_cache;


///////////////////////////////////////////////////////////////////////////////
void
pipeline_cache::merge (const device &dev,
                       const pipeline_cache *first,
                       const pipeline_cache *last)
{
    CHECK_GE (last, first);
    auto err = vkMergePipelineCaches (dev.native (), native (), cruft::cast::lossless<uint32_t> (last - first), &first->native ());
    error::try_code (err);
}


///////////////////////////////////////////////////////////////////////////////
size_t
pipeline_cache::get (const device &dev, void *dst, size_t len) const
{
    auto err = vkGetPipelineCacheData (dev.native (), native (), &len, dst);
    error::try_code (err);
    return len;
}
