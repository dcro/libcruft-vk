/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#include "./event.hpp"

#include "./device.hpp"

using cruft::vk::event;


///////////////////////////////////////////////////////////////////////////////
bool
event::is_set (const device &dev) const
{
    auto res = vkGetEventStatus (dev.native (), native ());
    error::try_code (res);
    return res == VK_EVENT_SET;
}


//-----------------------------------------------------------------------------
void
event::set (const device &dev)
{
    auto err = vkSetEvent (dev.native (), native ());
    error::try_code (err);
}


//-----------------------------------------------------------------------------
void
event::reset (const device &dev)
{
    auto err = vkResetEvent (dev.native (), native ());
    error::try_code (err);
}
