/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./traits.hpp"

#include "./instance.hpp"


///////////////////////////////////////////////////////////////////////////////
#if 0
#define INSTANTIATE(KLASS, NAME, FUNC) \
decltype(FUNC)* cruft::vk::life_traits<KLASS>::NAME = nullptr

INSTANTIATE(VkDebugReportCallbackEXT, create,  vkCreateDebugReportCallbackEXT);
INSTANTIATE(VkDebugReportCallbackEXT, destroy, vkDestroyDebugReportCallbackEXT);
#endif


///////////////////////////////////////////////////////////////////////////////
void
cruft::vk::load_traits (instance &i)
{
#if 0
    #define LOAD(KLASS,NAME,FUNC) \
    try { \
        cruft::vk::life_traits<KLASS>::NAME = i.proc<decltype(&FUNC)> (#FUNC); \
    } catch (const vk::invalid_argument&) \
    { ; }

    LOAD(VkDebugReportCallbackEXT, create,  vkCreateDebugReportCallbackEXT);
    LOAD(VkDebugReportCallbackEXT, destroy, vkDestroyDebugReportCallbackEXT);
#else
    (void)i;
#endif
}
