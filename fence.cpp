/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#include "./fence.hpp"

#include "./device.hpp"

#include <cruft/util/cast.hpp>
#include <cruft/util/debug/assert.hpp>

using cruft::vk::fence;


///////////////////////////////////////////////////////////////////////////////
bool
fence::is_ready (const device &dev)
{
    auto res = vkGetFenceStatus (dev.native (), native ());
    error::try_code (res);
    return res == VK_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////
void
fence::reset (const device &dev, fence *first, fence *last)
{
    CHECK_LE (first, last);

    cruft::vk::error::try_func (
        vkResetFences, dev.native (), cruft::cast::lossless<uint32_t> (last - first), &first->native ()
    );
}


//-----------------------------------------------------------------------------
void
fence::wait (const device &d, fence *first, fence *last, uint64_t timeout)
{
    CHECK_LE (first, last);

    cruft::vk::error::try_func (
        vkWaitForFences,
        d.native (), cruft::cast::lossless<uint32_t> (last - first), &first->native (), VK_FALSE, timeout
    );
}


//-----------------------------------------------------------------------------
void
fence::wait_all (const device &d, fence *first, fence *last, uint64_t timeout)
{
    CHECK_LE (first, last);

    cruft::vk::error::try_func (
        vkWaitForFences,
        d.native (), cruft::cast::lossless<uint32_t> (last - first), &first->native (), VK_TRUE, timeout
    );
}
