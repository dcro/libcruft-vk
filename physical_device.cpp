/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./physical_device.hpp"

#include "./except.hpp"
#include "./surface.hpp"

using cruft::vk::physical_device;


///////////////////////////////////////////////////////////////////////////////
std::set<std::string>
physical_device::extensions (void) const
{
    auto values = error::try_values (
        vkEnumerateDeviceExtensionProperties, native (), nullptr
    );

    std::set<std::string> strings;
    for (const auto &i: values)
        strings.emplace (i.extensionName);
    return strings;
}


///////////////////////////////////////////////////////////////////////////////
VkPhysicalDeviceProperties
physical_device::properties (void) const
{
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties (native (), &props);
    return props;
}


//-----------------------------------------------------------------------------
VkPhysicalDeviceFeatures
physical_device::features (void) const
{
    VkPhysicalDeviceFeatures feat;
    vkGetPhysicalDeviceFeatures (native (), &feat);
    return feat;
}


//-----------------------------------------------------------------------------
VkPhysicalDeviceMemoryProperties
physical_device::memory_properties (void) const
{
    return error::try_query (
        vkGetPhysicalDeviceMemoryProperties, native ()
    );
}


///////////////////////////////////////////////////////////////////////////////
std::vector<VkQueueFamilyProperties>
physical_device::queue_families (void) const
{
    return error::try_values (
        vkGetPhysicalDeviceQueueFamilyProperties, native ()
    );
}


///////////////////////////////////////////////////////////////////////////////
std::string
physical_device::name (void) const
{
    return properties ().deviceName;
}
