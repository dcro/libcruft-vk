/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_COMMAND_POOL_HPP
#define CRUFT_VK_COMMAND_POOL_HPP

#include "./object.hpp"

namespace cruft::vk {
    struct command_pool : public owned<command_pool,device> {
        using owned::owned;

        void reset (const device&, VkCommandPoolResetFlags);
    };
}

#endif
