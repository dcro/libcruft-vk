/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016, Danny Robson <danny@nerdcruft.net>
 */

#include "./device.hpp"

#include "./physical_device.hpp"
#include "./queue.hpp"

#include <cruft/util/cast.hpp>
#include <cruft/util/debug/assert.hpp>

using cruft::vk::device;


///////////////////////////////////////////////////////////////////////////////
device::device (const physical_device &phys,
                const VkDeviceCreateInfo &info):
    descendant (phys, &info, nullptr)
{ ; }


///////////////////////////////////////////////////////////////////////////////
cruft::vk::queue
device::queue (uint32_t idx)
{
    return ::cruft::vk::queue (*this, idx, 0);
}


///////////////////////////////////////////////////////////////////////////////
void
device::flush (const VkMappedMemoryRange *first,
               const VkMappedMemoryRange *last)
{
    CHECK_LE (first, last);

    auto err = vkFlushMappedMemoryRanges (native (), cruft::cast::lossless<uint32_t> (last - first), first);
    error::try_code (err);
}


//-----------------------------------------------------------------------------
void
device::invalidate (const VkMappedMemoryRange *first,
                    const VkMappedMemoryRange *last)
{
    CHECK_LE (first, last);

    auto err = vkInvalidateMappedMemoryRanges (native (), cruft::cast::lossless<uint32_t> (last - first), first);
    error::try_code (err);
}

