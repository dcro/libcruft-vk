/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_VK_SWAPCHAIN_HPP
#define CRUFT_VK_SWAPCHAIN_HPP

#include "./object.hpp"

namespace cruft::vk {
    struct swapchain : public owned<swapchain,device> {
        using owned::owned;
    };


    function_wrapper<decltype(&vkGetSwapchainImagesKHR)> images;
};

#endif
