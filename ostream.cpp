/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2017, Danny Robson <danny@nerdcruft.net>
 */

#include "ostream.hpp"

#include "instance.hpp"
#include "physical_device.hpp"

#include <cruft/util/iterator/infix.hpp>

#include <iomanip>
#include <ostream>


///////////////////////////////////////////////////////////////////////////////
std::ostream&
operator<< (std::ostream &os, VkExtent2D val)
{
    return os << "[ " << val.width << ", " << val.height << " ]";
}


//-----------------------------------------------------------------------------
std::ostream&
operator<< (std::ostream &os, VkExtent3D val)
{
    return os << "[ " << val.width << ", " << val.height << ", " << val.depth << "]";
}


#if 0
//-----------------------------------------------------------------------------
std::ostream&
operator<< (std::ostream &os, VkQueueFlags val)
{
    const char *names[4] = {};
    int cursor = 0;

    if (val & VK_QUEUE_GRAPHICS_BIT)        names[cursor++] = "GRAPHICS";
    if (val & VK_QUEUE_COMPUTE_BIT)         names[cursor++] = "COMPUTE";
    if (val & VK_QUEUE_TRANSFER_BIT)        names[cursor++] = "TRANSFER";
    if (val & VK_QUEUE_SPARSE_BINDING_BIT)  names[cursor++] = "SPARSE_BINDING";

    return os << "[ " << cruft::make_infix (cruft::make_view (names, names + cursor)) << " ]";
}
#endif


///////////////////////////////////////////////////////////////////////////////
static
std::ostream&
write_version (std::ostream &os, uint32_t version)
{
    static constexpr uint32_t MAJOR_BITS = 10;
    static constexpr uint32_t MINOR_BITS = 10;
    static constexpr uint32_t PATCH_BITS = 12;
    static_assert (MAJOR_BITS + MINOR_BITS + PATCH_BITS == sizeof (version) * 8);

    uint32_t major, minor, patch;

    patch = version & ((1 << PATCH_BITS) - 1); version >>= PATCH_BITS;
    minor = version & ((1 << MINOR_BITS) - 1); version >>= MINOR_BITS;
    major = version & ((1 << MAJOR_BITS) - 1); version >>= MAJOR_BITS;

    return os << +major << '.' << +minor << '.' << +patch;
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::vk::operator<< (std::ostream &os, const instance &i)
{
    os << "{ extensions: [ ";

    const auto &extensions = i.extensions ();
    std::copy (std::cbegin (extensions),
               std::cend   (extensions),
               cruft::iterator::infix_iterator<std::string> (os, ", "));

    return os << " ] }";
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
operator<< (std::ostream &os, const VkLayerProperties &val)
{
    return os << "{ name: '" << val.layerName << "'"
              << ", specVersion: " << val.specVersion
              << ", implementationVersion: " << val.implementationVersion
              << ", description: '" << val.description << "'"
              << " }";
};


///////////////////////////////////////////////////////////////////////////////
std::ostream&
operator<< (std::ostream &os, VkPhysicalDeviceType t)
{
    switch (t) {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER:             return os << "other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:    return os << "integrated-gpu";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:      return os << "discrete-gpu";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:       return os << "virtual-gpu";
    case VK_PHYSICAL_DEVICE_TYPE_CPU:               return os << "cpu";
    }

    return os << "unknown-" << +static_cast<std::underlying_type_t<decltype(t)>> (t);
}


//-----------------------------------------------------------------------------
std::ostream&
operator<< (std::ostream &os, const VkPhysicalDeviceLimits &l)
{
    return os
        << "{ max_image: [ " << l.maxImageDimension1D
                     << ", " << l.maxImageDimension2D
                     << ", " << l.maxImageDimension3D
                     << " ]"
        << " }";
}


//-----------------------------------------------------------------------------
std::ostream&
operator<< (std::ostream &os, const VkPhysicalDeviceProperties &p)
{
    os << "{ api: '";    write_version (os, p.apiVersion) << "'";
    os << ", driver: '"; write_version (os, p.driverVersion) << "'";
    os << ", vendor: 0x" << std::hex << std::setw (4) << std::setfill ('0') << p.vendorID << std::dec;
    os << ", device: 0x" << std::hex << std::setw (4) << std::setfill ('0') << p.deviceID << std::dec;
    os << ", name: '"  << p.deviceName << "'";
    os << ", type: "   << p.deviceType;
    os << ", limits: " << p.limits;
    os << " }";

    return os;
}


//-----------------------------------------------------------------------------
std::ostream&
operator<< (std::ostream &os, const VkPhysicalDeviceFeatures &f)
{
    os << "[ ";
    if (f.geometryShader) os << "GEOMETRY_SHADER ";
    return os << "]";
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::vk::operator<< (std::ostream &os, const physical_device &d)
{
    return os << "physical_device { "
              << "extensions: [ " << cruft::iterator::make_infix (d.extensions ()) << " ]"
              << ", properties: " << d.properties ()
              << ", features: " << d.features ()
              << ", queues: [ " << cruft::iterator::make_infix (d.queue_families ()) << " ]"
              << " }";
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
operator<< (std::ostream &os, const VkQueueFamilyProperties &val)
{
    return os << "{ flags: "       << val.queueFlags
              << ", count: "       << val.queueCount
              << ", granularity: " << val.minImageTransferGranularity
              << " }";
};
