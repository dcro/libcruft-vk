/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./object.hpp"

#include "./instance.hpp"

#include "./physical_device.hpp"

using cruft::vk::object;
using cruft::vk::enumerated;


///////////////////////////////////////////////////////////////////////////////
#define OBJECT(T) template struct cruft::vk::object<cruft::vk::T>;
VK_TYPE_MAP (OBJECT)


///////////////////////////////////////////////////////////////////////////////
#define DESCENDANT(T) template struct cruft::vk::descendant<cruft::vk::T>;
VK_DESCENDANT_TYPE_MAP (DESCENDANT)

