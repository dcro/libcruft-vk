/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2016-2017, Danny Robson <danny@nerdcruft.net>
 */

#include "./instance.hpp"

#include "./traits.hpp"

using cruft::vk::instance;


///////////////////////////////////////////////////////////////////////////////
static const VkInstanceCreateInfo DEFAULT_INFO = {
    .sType = cruft::vk::structure_type_v<VkInstanceCreateInfo>,
    .pNext = nullptr,
    .flags = {},

    .pApplicationInfo = nullptr,

    .enabledLayerCount = 0,
    .ppEnabledLayerNames = nullptr,

    .enabledExtensionCount = 0,
    .ppEnabledExtensionNames = nullptr
};


//-----------------------------------------------------------------------------
instance::create_info_t::create_info_t ():
    create_info_t (DEFAULT_INFO)
{ ; }


//-----------------------------------------------------------------------------
instance::create_info_t::create_info_t (const VkInstanceCreateInfo &base):
    VkInstanceCreateInfo (base)
{ ; }


///////////////////////////////////////////////////////////////////////////////
instance::instance ():
    instance (create_info_t ())
{ ; }


//-----------------------------------------------------------------------------
instance::instance (const cruft::view<const char **> layers,
                    const cruft::view<const char **> extensions):
    instance (VkInstanceCreateInfo {
        .sType = cruft::vk::structure_type_v<VkInstanceCreateInfo>,
        .pNext = nullptr,
        .flags = {},
        .pApplicationInfo = nullptr,
        .enabledLayerCount = static_cast<uint32_t> (layers.size ()),
        .ppEnabledLayerNames = layers.data (),
        .enabledExtensionCount = static_cast<uint32_t> (extensions.size ()),
        .ppEnabledExtensionNames = extensions.data ()
    })
{ ; }


//-----------------------------------------------------------------------------
instance::instance (const create_info_t &info):
    root (&info, nullptr)
{ ; }



///////////////////////////////////////////////////////////////////////////////
std::set<std::string>
instance::extensions (void) const
{
    auto values = error::try_values (
        vkEnumerateInstanceExtensionProperties, nullptr
    );

    std::set<std::string> strings;
    for (const auto &i: values)
        strings.insert (i.extensionName);
    return strings;
}


///////////////////////////////////////////////////////////////////////////////
std::vector<VkLayerProperties>
instance::available_layers (void)
{
    return error::try_values (vkEnumerateInstanceLayerProperties);
}
