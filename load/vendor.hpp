#pragma once

#include <cruft/vk/vk.hpp>
#include <cruft/vk/load/vtable.hpp>

#include <cruft/util/library.hpp>
#include <cruft/util/std.hpp>

#include <filesystem>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////////
namespace cruft::vk::load {
    struct icd_t {
        std::string file_format_version;
        struct {
            std::filesystem::path library_path;
            std::string api_version;
        } icd;
    };


    std::vector<icd_t>
    enumerate [[gnu::visibility("default")]] (void);


    struct vendor_table {
        VkResult (*vk_icdNegotiateLoaderICDInterfaceVersion)(u32*) = nullptr;
        void* (*vk_icdGetInstanceProcAddr) (VkInstance, char const*) = nullptr;
        void* (*vk_icdGetPhysicalDeviceProcAddr) (VkInstance, char const*) = nullptr;
    };

    class [[gnu::visibility("default")]] vendor {
    public:
        vendor (icd_t const&);
        vendor (::cruft::library &&);

    private:
        ::cruft::library m_library;

    public:
        vendor_table vtable;
        instance_table itable;
        u32 version = 0;
    };
}
