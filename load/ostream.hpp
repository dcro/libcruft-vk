/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "fwd.hpp"

#include <iosfwd>

namespace cruft::vk::load {
    std::ostream& operator<< [[gnu::visibility("default")]] (std::ostream&, icd_t const&);
}
