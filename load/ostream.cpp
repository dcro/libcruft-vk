/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "ostream.hpp"

#include "vendor.hpp"


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::vk::load::operator<< (std::ostream &os, icd_t const &value)
{
    return os << "{ file_format_version: '" << value.file_format_version << "'"
              << ", icd: { library_path: '" << value.icd.library_path    << "'"
              <<        ", api_version: '"  << value.icd.api_version     << "'"
              << " } }";
};
