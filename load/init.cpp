/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */


#include <cruft/vk/load/vtable.hpp>
#include <cruft/vk/load/vendor.hpp>

#include <vector>


///////////////////////////////////////////////////////////////////////////////
// We need to ensure the tables are loaded before anything much gets called.
//
// Given we have a global vtable we'll install a default table that hooks the
// function(s) that a client will call first off. This function will:
//   * find the various ICDs on the system,
//   * install a vtable that forwards calls by default,
//   * and then forwards the current call on to this newly installed vtable


///////////////////////////////////////////////////////////////////////////////
static void setup_tables (void)
{
    static auto libraries = cruft::vk::load::enumerate ();
    static cruft::vk::load::vendor v (libraries[0]);

    cruft::vk::load::v_table = &v.vtable;
}


///////////////////////////////////////////////////////////////////////////////
static void*
_vk_icdGetInstanceProcAddr (VkInstance instance, char const *name)
{
    setup_tables ();
    return cruft::vk::load::v_table->vk_icdGetInstanceProcAddr (instance, name);
}


///////////////////////////////////////////////////////////////////////////////
static cruft::vk::load::vendor_table const v_initialiser {
    .vk_icdGetInstanceProcAddr = _vk_icdGetInstanceProcAddr,
};


cruft::vk::load::vendor_table const *cruft::vk::load::v_table = &v_initialiser;
